Article 19 of the Universal Declaration of Human Rights asserts the right for
all people ``to seek, receive and impart information and ideas through any media
regardless of frontiers."~\cite{udhr} And
indeed, around the world, people are trying to safely speak out about their
situations, and safely learn what others have to say. This free and open
communication on the internet is critical to the growth of strong communities
and societies. As the American Library Association wrote in 1953, ``the
suppression of ideas is fatal to a democratic society."~\cite{ftr}

At the same time, nation-state censors and others attempt to \emph{deny} this
communication, threatening the health and safety of communities. In this
project, we leverage our past work on the Tor anonymity system and the
pluggable transport ecosystem to explore and analyze techniques to
detect, understand, and mitigate these denials of service. In this first
report, we present the
current state-of-play in these areas, as well as some research
directions and plans for future work. In subsequent reports, we will describe
our findings and our progress with work on the project.

This report is organized as follows: the remainder of this section gives a
brief overview of Tor and describes how it is used to evade censorship;
Section~\ref{chap:better-pts} describes pluggable transports and how Tor
Browser uses them; Section~\ref{chap:more-apps} discusses applications that
currently use the Tor network as well as some challenges to using popular apps
in censored locations; Section~\ref{chap:perf-bad-networks} discusses onion
routing client performance on networks with resource constraints;
Section~\ref{chap:measurement} outlines how we measure key metrics about
the Tor network, including censorship of Tor;
Section~\ref{chap:network-defense} describes some attacks on the Tor network
and how we defend against them; and Section~\ref{chap:stronger-research}
discusses building a stronger relationship between the Tor Project and academic
researchers.

\subsection{About Tor}
\label{intro-about-tor}

The Tor anonymity system~\cite{tor-design} protects internet users from
tracking, surveillance, and censorship. The Tor network is made up of thousands
of volunteer-run \emph{relays}---servers that are usually located in data
centers---distributed across the world that enable users to make private
connections to services on the internet. Currently, the vast majority of
connections to the Tor network are made using the Tor Browser. But a growing
number of applications use the Tor network (see Section~\ref{apps-that-use-tor}
for a list), and we expect that many more will do so in the future.

When Tor Browser starts up, it connects to a single relay in the Tor network,
known as the \emph{guard} relay. The connection to the guard is always
encrypted, and all web connections are passed through this fully-encrypted
connection so that third parties (such as an ISP) observing the user's internet
connection cannot see which website the user is visiting.

Then, for each connection to a public website, Tor builds a tunnel through the
guard relay and two other randomly-chosen Tor relays. Connections from Tor
Browser thus pass, encrypted, through three Tor relays before exiting from the
Tor network onto the public internet. The way the encryption works means that
the guard relay can see the address of the computer making the request, but not
the website the user is visiting; the middle relay can see the address of the
guard relay and the address of the third relay (called the \emph{exit relay});
and the exit relay can see the address of the middle relay as well as the
destination website.

A Tor connection through a guard relay, a middle relay, and an exit relay
is called a Tor \emph{circuit}. Tor relays are usually part of many Tor
circuits and relay traffic for many Tor users at the same time.

Tor's \emph{directory authorities} are a group of trusted servers that keep
track of all of the relays that are currently able to help users protect their
privacy. Directory authorities periodically publish a list of the addresses of
these available relays, and applications such as Tor Browser choose relays from
this list when they connect to the Tor network.

\subsection{Tor and censorship} 
%\label{intro-tor-censorship}

Governments and other censors use a variety of tools to deny people access to
the free and open internet. As a result, systems for getting around censorship,
including Tor, must adopt a variety of measures as well.

Because the connection between Tor Browser and the guard relay is encrypted,
parties observing a user's internet connection (such as an ISP) cannot
selectively block connections based on their content or destination. Some
governments and ISPs have responded to this censorship evasion by attempting to
prevent users from connecting to the Tor network in the first place. 

One way to do this blocking is to prevent people from downloading the Tor
Browser by blocking connections to the Tor Project website and its mirrors. We
responded to this concern by launching the \emph{GetTor service}~\cite{gettor},
which allows users to make a request via email or twitter. The service responds
with a link to a cloud storage site that the user can download Tor Browser
from.

Another way to block access to the Tor network is to block connections to
addresses published in the publicly available list of Tor relays. Tor
\emph{bridges} are Tor relays that are not in the public list, which makes
blocking them more difficult. How to distribute bridge addresses to users while
keeping them from the censors is discussed in Sections~\ref{bridge-distrib} and
\ref{browser-censorship}.

More sophisticated censors use \emph{deep packet inspection} (DPI) to recognize
and block connections. This method examines the data moving across the internet
and tries to determine whether it's Tor traffic. To counter this blocking, Tor
uses \emph{pluggable transports}. Pluggable transports disguise the
traffic between the user and a Tor bridge so that it doesn't look like Tor
traffic. Pluggable transports are discussed in Sections~\ref{pt-types} and
\ref{browser-censorship}.
