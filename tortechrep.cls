% LaTeX article-subclass for Tor Project technical reports.
% Original version, 2012/07/19  Zack Weinberg.
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tortechrep}[2012/07/19 Article specialization for Tor
                                      technical reports]

% 12 point, letter paper, pass other options down to article.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[12pt,letterpaper]{article}

% Two inches of margin area, with geometry's default margin ratios.
\RequirePackage{geometry}
\geometry{textwidth=6.5in,textheight=9in}

% These packages must be loaded before any fonts.
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}

% Font choices up for debate, but I think these are inarguably
% superior to Computer Modern, and should be available with any
% recent version of TeX Live.
\RequirePackage[charter]{mathdesign}
\RequirePackage{inconsolata}

% This package must be loaded after any fonts.
\RequirePackage[final]{microtype}

% These packages must be loaded in this order.
\RequirePackage[hyphens]{url}
\RequirePackage{color}
\RequirePackage[colorlinks=true,urlcolor=blue,citecolor=dgreen]{hyperref}
\hypersetup{pageanchor=false}
\definecolor{dgreen}{RGB}{0,127,0}

% Title customization
\RequirePackage{titling}
\def\@thecontact{}
\def\@thereportid{\unskip}
\def\@thesubtitle{}
\newcommand{\contact}[1]{\def\@thecontact{#1}}
\newcommand{\reportid}[1]{\def\@thereportid{#1}}
\newcommand{\subtitle}[1]{\def\@thesubtitle{\\\large--- #1 ---}}
\let\@origpostauthor=\@bspostauthor
\postauthor{\\{\normalsize\ttfamily\@thecontact}\@origpostauthor}
\let\@origposttitle=\@bsposttitle
\posttitle{\@thesubtitle\@origposttitle}
\let\@origpredate=\@bspredate
\predate{\@origpredate Tor Tech Report \@thereportid\\}

% TBD: do we want something cleverer?
\bibliographystyle{plain}
