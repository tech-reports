  rm( list = ls() )
  setwd('/Users/know/Desktop/tor analytics/')
  library(car)    # for pretty plots
  
  library(plyr) # for renaming columns
  source("colortitles.R")
  #######################################################
  
  D <- read.csv('bandwidth-clean.csv')[c('day','advbw','bwread','date')]
  #D <- rename(D, c("date"="day") )
  
  # convert from B/s to MiB/s
  D$advbw <- D$advbw / 1048576.0
  D$bwread <- D$bwread / 1048576.0
  
  ## Plot the "Congestion" --- read / advertised
  ##################################################################
  D[c("congestion")] <- NA
  D$congestion <- D$bwread / D$advbw
  
  ####### Remove some outliers ####################################################
  Dother <- subset(D, congestion <= 0.01 )
  D <- subset(D, congestion > 0.01 )
  # drop all points between days [1200,1310] with congestion BELOW 0.55
  outliers <- subset(D, ( 1200 <= day & day <= 1310 & congestion <=0.55) | (2258 <= day & day <= 2320 & congestion <= 0.47) )
  
  D <- subset(D, !( 1200 <= day & day <= 1310 & congestion <=0.55) )
  D <- subset(D, !(2258 <= day & day <= 2320 & congestion <= 0.47) )
  #################################################################################
  
  ####### Put into groups ####################################################
  cut_off1 <- 2173                    # delta=2173 is date 2013-10-08
  cut_off2 <- 2413                    # delta=2413 is date 2014-06-05
  g1 <- subset(D, day <= cut_off1 & congestion >= 0.5 )
  g2 <- subset(D, cut_off1 < day & day <= cut_off2 )
  g3 <- subset(D, cut_off2 < day )
  #################################################################################
  
  par(las=1)
  plot(D$day, D$congestion,
       col='black', pch='.', cex=0.6, ylim=c(0.35,0.8), 
       #xlab="Year", ylab="used bandwidth / capacity bandwidth", xaxt='n', yaxs="i")
       xlab="Year", ylab="NUR", xaxt='n', yaxs="i")
  
  # plot the three groups
  points( g1$day, g1$congestion, col='red', pch=20, cex=0.6 )
  points( g2$day, g2$congestion, col='blue', pch=20, cex=0.6 )
  points( g3$day, g3$congestion, col='green', pch=20, cex=0.6 )
  
  # plot the outliers
  #points( outliers$day, outliers$congestion, col='black', pch=1, cex=0.6 )
  
  
  ####### Set the pretty X-axis ############################
  par(las=1)
  YearLabels=seq(from=2008,to=2014,by=1)
  YearLocations=c(66,432,797,1162,1527,1893,2258)
  axis(1,at=YearLocations,labels=YearLabels )
  ##########################################################
  
  
  ## Plot the three best-fit lines
  #################################################################
  g1 <- subset(g1, day >= 1200)
  fit_D <-  lm( congestion ~ day, data=D )
  fit_g1 <- lm( congestion ~ day, data=g1 )
  fit_g2 <- lm( congestion ~ day, data=g2 )
  fit_g3 <- lm( congestion ~ day, data=g3 )
  
  
  # add the line segment for the predicted pine
  segments( min(g1$day), predict(fit_g1, data.frame(day=min(g1$day))),
            max(g1$day), predict(fit_g1, data.frame(day=max(g1$day))),
            col="black", lty=1, lwd=3 )
  
  segments( min(g2$day), predict(fit_g2, data.frame(day=min(g2$day))),
            max(g2$day), predict(fit_g2, data.frame(day=max(g2$day))),
            col="black", lty=1, lwd=3 )
  
  segments( min(g3$day), predict(fit_g3, data.frame(day=min(g3$day))),
            max(g3$day), predict(fit_g3, data.frame(day=max(g3$day))),
            col="black", lty=1, lwd=3 )
  
  
  # Add a point highlighting the beginning of the line
  points( min(g1$day), predict(fit_g1, data.frame(day=min(g1$day))), col="black", pch=15, cex=1.3)
  points( min(g2$day), predict(fit_g2, data.frame(day=min(g2$day))), col="black", pch=15, cex=1.3)
  points( min(g3$day), predict(fit_g3, data.frame(day=min(g3$day))), col="black", pch=15, cex=1.3)
  
  
  
  
  ## Set the Title and Legend
  #################################################################
  #title("Better: channel utilization in three distinct, flat stages")
  legend_text = c('2010-04-30 to 2013-10-08', '2013-10-09 to 2014-06-05', '2014-06-06 to present')
  legend( "bottomleft", legend=legend_text, inset=0.05, pch=c(20,20,20), col=c('red','blue','green') ) 
  
  
  

  
