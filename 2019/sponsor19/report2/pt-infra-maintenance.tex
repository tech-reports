\label{sec:pts-infra-maint}

Pluggable transports~\cite{tor-pt-spec} help people evade censorship by
disguising the traffic between a Tor client, such as Tor browser, and a bridge
so that it doesn't look like Tor protocol traffic. This is often necessary in
locations where the censor uses deep packet inspection (DPI) to examine traffic
as it flows across the network. 

Improving the pluggable transport ecosystem is ongoing work with many
concurrent lines of research: improving the interface between pluggable
transports and Tor; enabling Tor Browser to use other censorship circumvention
tools when available; improving each of the currently viable pluggable
transports; finding more entities that are willing to support domain fronting
in their networks; and maintaining connections with academic research groups so
that we stay up to date on emerging work on pluggable transports. In addition to
these areas, we must also continue to research and develop the next generation
of pluggable transports; Sections~\ref{sec:pt-snowflake}
and~\ref{sec:pt-other-ngen} describe that work.

\subsection{Improve logging and status messages \bluesmallcaps{partially completed}}

\label{sec:pt-log-message-passing}

To make it easier for developers to write and maintain high quality pluggable
transports and to help us better understand when and how pluggable transports
get blocked by censors, we added two new features to support better logging and
message passing capabilities for pluggable transports.

The first feature enables pluggable transports to log directly to Tor's log
files. Previously, pluggable transports were responsible for their own logging,
which meant developers needed to analyze both sets of logs. Trying to determine
the order of events across the log files could be error prone. With the release
of this feature all of the logs will be in one file, written in a standard
form. We are currently defining the format for the log messages a pluggable
transport can pass to the Tor process for logging.

The second feature implements a new status handler, making it possible for
pluggable transports to report status information back to the Tor control port
for use by clients---such as Tor Browser and OONI Probe. This information might
help us identify that a particular pluggable transport is being blocked and how
the censor is blocking it.

The next step to make use of the status handler is to define key-value pairs in
the pluggable transport specification~\cite{tor-pt-spec} to allow pluggable
transports to report useful information.

\maybeurl{https://bugs.torproject.org/28180} \maybelinebreak
\maybeurl{https://bugs.torproject.org/28181} \maybelinebreak
\maybeurl{https://bugs.torproject.org/25502}

Other recent related work extended the status messages produced during
bootstrap process---the process of connecting to the Tor network and building a
circuit---to distinguish between connections to a Tor relay, a pluggable
transport, and a simple proxy used to bypass a firewall.

Because a pluggable transport can, itself, use a firewall bypass proxy, we
should consider reordering the bootstrap status messages to take this fact into
account. A possible order follows:

\begin{itemize}

	\item \texttt{conn\_pt} - Connecting to pluggable transport
	\item \texttt{conn\_proxy} - Connecting to proxy
	\item \texttt{conn\_proxy\_done} - Connected to proxy
	\item \texttt{conn\_pt\_done} - Connected to pluggable transport

\end{itemize}

\maybeurl{https://bugs.torproject.org/28925} \maybelinebreak
\maybeurl{https://bugs.torproject.org/28930}

\subsection{Specify which outbound interface to use}

Tor can be configured to use a particular outbound network interface, but there
is currently no way to tell a pluggable transport that it should use that
interface. We need a mechanism in tor to tell pluggable transports which
interface to use. This will also entail work on the pluggable transport side to
look for and honor the preference.

\maybeurl{https://bugs.torproject.org/5304}

\subsection{Make pluggable transports more mobile friendly}

Currently, when a pluggable transport has been launched but is not being used,
it remains running in the background, sometimes even making network
connections. In order to use fewer resources and thus be more suitable for use
on mobile devices, pluggable transports should become dormant when not in use.
We need to analyze the pluggable transports protocol to look for a way to do
this.

\maybeurl{https://bugs.torproject.org/29283}

We also need to assess how dormant mode interacts with message passing between
pluggable transports and Tor.

\maybeurl{https://bugs.torproject.org/28849}

\subsection{Better testing for pluggable transports}

We need to ensure that we have good test coverage for pluggable transports.
This includes getting common pluggable transports included in our Chutney
continuous integration testing process. In addition, we need to ensure that new
pluggable transports, as well as currently deployed, viable ones, have a high
degree of test coverage. We should also encourage Tor developers to alpha-test
new and updated pluggable transports so that we can discover issues before
release. Finally, we should ensure that we adequately test IPv6 configurations
for all current and future pluggable transports.

\maybeurl{https://bugs.torproject.org/29280} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29267} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29024} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29258} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29259} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29274}

\subsubsection{Reachability self testing}

Bridges that offer pluggable transports don't currently have a way to test
their reachability. This means that operators may not be able to tell if their
bridge is unreachable, for example because of NAT. We need a mechanism to test
the reachability of these bridges, particularly those offering obfs4.

The long term solution is to have Tor run the test. A potential design is to
have the bridge's Tor client establish a TCP connection with its obfs4 port.
Tor can then warn the operator in its log file if the test fails. (We note
that this won't solve the problem for any future pluggable transports that run
over UDP.)

In the shorter term, while we wait for implementation and deployment, we will
create and host a simple web page that asks for an IP address and a port
as input. The service then tries to establish a TCP connection to the given
tuple, and lets the user know if it succeeded or failed. The service doesn't
need to log or remember anything, and we can run it on the host that also runs
BridgeDB.

\maybeurl{https://bugs.torproject.org/30472}

\subsection{Enable Tor Browser to use other circumvention tools}

Sometimes when access to the Tor network is blocked, other censorship
circumvention tools may still work in the censored location. In such a case,
Tor Browser might be able to detect the other tool and use it to connect the
user to the desired website.

Although these tools generally come with trade-offs related to performance,
reliability, and safety, it would be useful to make it easy for Tor Browser to
route traffic through them if they are installed. These options could be
offered to the user in the pluggable transport selection menu.

One point to consider is that many of these tools attempt to hide their
presence to keep censors from finding them, so this work would require
cooperation with the makers of the tools.

\maybeurl{https://bugs.torproject.org/28556}

\emph{Notes:}
\begin{enumerate}
	\item Create a list of the censorship circumvention tools that we'd like Tor Browser to use when access to the Tor network is blocked.

	\item Contact the makers of these tools and ask if they'd like to collaborate with us by offering an API that Tor Browser could use to route traffic through their systems.

	\item Design and implement an update to Tor Launcher so that it can configure Tor Browser to use the tools and add them as options in Tor Browser's pluggable transport selection menu.
\end{enumerate}

\subsection{Maintain currently deployed pluggable transports}

Although it is important to look ahead to the next generation of pluggable
transports (see Sections~\ref{sec:pt-snowflake} and~\ref{sec:pt-other-ngen}), we
must also attend to the maintenance of currently deployed pluggable transports
that we believe work well, such as obfs4proxy and meek~\cite{meek}.  

All of our work on pluggable transports requires that we better understand the
current censorship landscape. Knowing what works and what doesn't work, in
which countries, as well as how censors block Tor, is essential to maintaining
current pluggable transports and designing new ones. For more information on
understanding how Tor is blocked, see Section~\ref{sec:tor-censorship}.

\subsubsection{obfs4proxy}

Obfs4proxy (or obfs4) is a pluggable transport that defends against active
probing attacks so it is resilient against blocking by many censored countries,
including, we believe, China. It is also possible that short of blocking obfs4,
censors may be degrading its performance. 

We need to test if obfs4 is actually being blocked or throttled in China and other censored locations. Our plan to do this follows:

\begin{enumerate}

	\item Set up several diverse, new, private obfs4 bridges for these tests.

	\item Write a client-side script that will test for obfs4 reachability,
	making sure to measure bandwidth for throttling. \bluesmallcaps{completed}

	\item Get set up on a virtual private server (VPS) to perform these tests
	ourselves initially.

	\item Contact users in China and other censored locations to run the
	scripts and send us the date they collect.

\end{enumerate}

If obfs4 is being blocked or its performance degraded, we should figure out how
and work to fix it. If it is not, then it is one of the viable pluggable
transports, so we need to allocate resources to fixing known issues and
maintaining its code base.

\subsubsection{Domain fronting}
\label{sec:domain-fronting}

Domain fronting---a system in which a client makes a connection to an
uncensored website and that website redirects the connection to a censored
website that is in the same domain---has proven resilient to blocking because
censors are unwilling to block the uncensored site. This technique has
generally been used within the domains of large cloud service providers and
requires their cooperation. Meek~\cite{meek} and Snowflake~\cite{snowflake}
both make use of domain fronting.

In 2018, reportedly~\cite{russia-amazon-google} because of political pressure
from Russia, Amazon and Google discontinued support for domain fronting. As
long as there are cooperating providers, domain fronting is difficult to defeat,
so finding other providers who will support it is an important tactic in the
fight against censorship. 

Other anti-censorship projects that use domain fronting may have lists of
companies that are still willing to support the technique. We need to
strengthen our collaboration with these projects to foster on-going knowledge
sharing.

Encrypted Server Name Indication (ESNI), which is an emerging draft
specification for encrypting SNI, the technology that allows a single web
server to host multiple websites, may offer some possibilities for domain
fronting as it gains wider deployment. We should monitor developments in this
area.

\subsubsection{meek}

As part of ongoing maintenance of meek, we fixed a bug where the meek client
for Tor Browser and its child Firefox process would continue to run after Tor
Browser was closed.

\subsection{Looking to the future}

\subsubsection{Maintain relationships with research groups}

One Tor Project priority is maintaining good relationships with academic
research groups that study privacy enhancing technologies in general, and
anonymity systems and censorship circumvention in particular. It is essential
that we stay current on research on new pluggable transports so that we can
continue to help our users maintain access to the free and open internet.

Members of our new anti-censorship team will meet with researchers both by
attending conferences and workshops, such as the USENIX Workshop on Free and
Open Communications on the Internet and the Privacy Enhancing Technologies
Symposium, and by visiting their institutions. We also plan to invite pluggable
transport researchers to our biannual Tor meetings.

\subsubsection{Consider ways to improve the pluggable transport specification}

We want to make it easier for developers and academics to design and
implement new pluggable transports and get them easily integrated with Tor so
that we can have a well-functioning pluggable transport integration pipeline.

\begin{itemize}

	\item Assess pain points with the current specification.
	
	\item Find out what new features pluggable transport developers would like
	to see.

	\item Assess the PT 2.1~\cite{pt-spec} draft specification to see where it
	differs from ours and decide whether we should include some of its
	features.
	
	\item Consider how bridge distribution should factor into the
	specification. For example, some transports such as meek and snowflake
	handle bridge information differently than transports whose bridges are
	distributed through BridgeDB. This results in a different interaction with
	Tor, and we might consider modifying the spec with the snowflake/broker
	model in mind.

	\maybeurl{https://bugs.torproject.org/29296}

\end{itemize}

In general, we should improve our communication with the pluggable transports
community to see what they need so that we can get more transports integrated
with Tor.

\maybeurl{https://bugs.torproject.org/29285}
