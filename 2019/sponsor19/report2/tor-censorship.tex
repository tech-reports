\label{sec:tor-censorship}

To inform our anti-censorship work, we need to better understand the places
where Tor is blocked and how censors are blocking it. We need up-to-date
information so that we can include good advice to users in Tor Launcher's
settings and help them when they ask for support. Knowing historical trends
will help us prioritize whether we should develop new pluggable transports or
new distribution methods for existing ones.

We currently have a variety of sources of information, but no current
comprehensive snapshot of which countries use which methods to block access to
the Tor network. We have data that OONI Probe collects, our Tor Metrics
timeline~\cite{tor-metrics-news}, anecdotal stories from our users and allies,
our censorship wiki, and a timeline published by researchers at UC
Berkeley~\cite{berkeley-censorship-timeline}. Some of this information is
current; some is older, but may still be accurate. In addition to this
information, there is other information that we could be collecting and we
describe some of it later in this section.

This comprehensive snapshot should tell us which countries block Tor and how
they do it: which ones block Tor directory authorities; which block the
public relays; which block the default bridges that are included in Tor
Browser; which collect bridge IP addresses from BridgeDB and block them;
which use DPI to detect and disconnect pluggable transport connections;
which degrade performance for protocols they don't like. When we create this
snapshot, we should also put into place a process for keeping it up to date.

\subsection{A Tor Browser that detects censorship}

In order to get a better understanding of which censored locations specific
pluggable transports work in, we need a tool for testing them. Some
deployed pluggable transports are quite large, and thus may be unreasonable to
integrate into OONI Probe, which is a mobile app. So we would like to make a
Tor Browser variant that cycles through the available pluggable transports and
reports to the user on which ones work. Of course, we'd also encourage users to
share this information with us to aid our work. 

A prerequisite for this is better reporting from Tor about the status of the
process of bootstrapping a connection to the Tor network. Recent work has
defined additional phases in the bootstrap process that allow us to distinguish
between connections to relays, pluggable transports, and proxies. Other work
enables a pluggable transport to send status and log messages to Tor which can
then be made available to Tor Browser and OONI Probe using the Tor control
port. These features will provide better information than we have historically
had and give us a better idea of what works where.

\maybeurl{https://bugs.torproject.org/23839} \maybelinebreak
\maybeurl{https://bugs.torproject.org/28531}

\subsection{Create and maintain an authoritative list of default bridges}

Default bridges are the bridges that are pre-configured in Tor Browser. The
list of these bridges is currently part of the Tor Browser software repository.
This list is used by other projects, notably OONI, but possibly others as well.
We want to create an authoritative list of these bridges, maintained by the
anti-censorship team, that is easy to parse so that it is usable by other
projects.

\maybeurl{https://bugs.torproject.org/30121}

\subsection{Assess status and reachability of default bridges}
\label{sec:default-bridges}

Default bridges are the set of bridges that are preconfigured in Tor Browser.
Currently, the set of bridges included is not updated regularly, and we don't
have a good understanding of whether they are still acting as bridges or from
which locations they may be censored. We need to implement a process for
scanning these bridges to answer these questions. 

\subsubsection{Default bridge reachability \bluesmallcaps{completed}}

A default bridge may become unreachable because the operator decides not to run
it anymore. We need an automated way of discovering when a default bridge
disappears. We have implemented sysmon monitoring of the set of default bridges.
The sysmon instance runs every 5 minutes, testing for reachability. If a check
fails twice in a row, that is if a bridge is unreachable for 5 minutes, it
sends an alert. This falls under the umbrella of adding monitoring for all of
our anti-censorship infrastructure.

\maybeurl{https://bugs.torproject.org/30152} \maybelinebreak
\maybeurl{https://bugs.torproject.org/30006}

\subsubsection{Default bridge blocking}

Default bridges may become unreachable because a censor is blocking them.
Currently, in some locations, OONI Probe tests whether these bridges are
reachable by checking if the TCP port is open. But this test only assesses
whether a bridge is reachable, which doesn't distinguish between a bridge that
is offline and one that is blocked. In addition, this test is subject to a
false positive if a bridge's TCP port is reachable, but it is blocked using DPI
of the actual Tor connection. We need better OONI tests that accurately report
whether these bridges work.

\maybeurl{https://bugs.torproject.org/29275}

As part of this work, we need to expose data about bridge reachability in a
form and location that is useful to Tor developers.

\maybeurl{https://bugs.torproject.org/12547}

We should also look into getting the default bridges tested using reachability
tests from external projects, such as Augur~\cite{Pearce-ssp17}, Censored
Planet~\cite{censored-planet}, or others.

\maybeurl{https://bugs.torproject.org/29277}

\subsection{Understand bridge load and blocking}
\label{sec:bridge-load-blocking}
 
In order to make good use of Tor bridges, we need to better understand their
usage and load, as well as how and when they are blocked by censors. Questions
we need to answer include: how are bridges being used; is there a relationship
between how BridgeDB distributes them and how much load they have; are they
overloaded; are they long or short lived; who runs them and where?

\subsection{Get more accurate counts of Tor users}
\label{sec:better-user-counts}
  
In order to recognize and respond to censorship events---that is, instances
when the Tor network is blocked from a particular location---we need accurate
counts of the number of people who use the Tor network.

As we detailed in Interim Report 1~\cite{tor-2018-11-001}, when we count Tor
users, we don't collect identifying information, but rather estimate the number
of users based on the number of requests made to directory authorities for the
list of relays in the network. Using this methodology, we estimate that we have
approximately 2 million Tor users a day.

Counting this way can be especially inaccurate in some censored countries. For
example, in some locations, you can successfully request the Tor
consensus from a directory authority, but when you attempt to make a connection
to a guard relay, the censor uses deep packet inspection (DPI) to detect the
Tor protocol and block the connection. In a situation like this, we count the
directory authority request as a user, but the client clearly was not able to
actually use Tor. This leads to inflated user counts exactly when a censor
blocks Tor.

Recent work~\cite{tor-usage-priv-measurement-imc18} that uses newer
privacy-preserving data collection, produced an estimate of about 8 million Tor
users in a day. We need to validate that research and consider whether we
should implement its methodology into our collection of user statistics.

\maybeurl{https://bugs.torproject.org/28555}
